var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var Request = require('request');

const args = process.argv;
var port = args[2];
var purchaseModel = require('./models/purchase.js');
var productModel = require('./models/product.js');
var router = express.Router();
var orders = [];
var productsAvaliable = [];


function random(min, max, unityParts) {

    var value = Math.random() * (max - min) + min;
    var roundedValue = Math.round(value * unityParts) / unityParts;
    if (unityParts == 1) {
        console.log("generated value is : [" + roundedValue + "]");
    }
    return roundedValue;
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}



function generateProducts(barcode, store, product, min, max) {
    var product1 = {};
    product1.name = product;
    product1.barcode = barcode;
    product1.price = random(min, max, 100);
    product1.store = store;
    return product1;
}
var barcode1 = makeid(12);
var barcode2 = makeid(12);
var barcode3 = makeid(12);
var barcode4 = makeid(12);
var barcode5 = makeid(12);

productsAvaliable.push(generateProducts(barcode1, port.toString(), "Clips:1000u", 300, 500));
productsAvaliable.push(generateProducts(barcode2, port.toString(), "Elasticos_15u", 300, 500));
productsAvaliable.push(generateProducts(barcode3, port.toString(), "Pack_de_resmas", 230, 550));
productsAvaliable.push(generateProducts(barcode4, port.toString(), "Pen_BIC_fino_100u", 430, 600));
productsAvaliable.push(generateProducts(barcode5, port.toString(), "Pen_BIC_ufino_100u", 300, 800));

router.route("/products").get(function (req, res) {
    var failed = true;
    console.log("Fetching products information");
    if (productsAvaliable.size == 0) {
        res.status(200).json("No products available");
        failed = false;
    } else {
        console.log(productsAvaliable);
        res.status(200).json({products: productsAvaliable});
        failed = false;
    }
    if (failed) {
        res.status(500).json("Something wrong happened Internal server error");
    }

});

function findProductByName(name) {
    var product;
    //console.log("products size is : " + productsAvaliable.length);
    for (i = 0; productsAvaliable.length; i++) {
        product = productsAvaliable[i];
        //console.log(product)
        if (product.name == name) {
            return product;
        }
    }
    return null;
}

router.route("/product/:name").get(function (req, res) {
    console.log("Fetching specific product information");
    name = req.params.name;
    setTimeout(function () {
        if (name) {
            console.log("name for performing the search is :[" + name + "]");
            var product = findProductByName(name);
            res.status(200).json(product);
        } else {
            console.log("user made an invalid request");
            res.status(200).json();
        }
    }, random(1000, 2000, 1));
});

router.route("/status/:id").get(function (req, res) {
    failed = true;
    console.log("Fetching products information");
    if (hashMap.size == 0) {
        res.status(200).send("No orders placed");
        failed = false;
    } else {
        let order = hashMap.get(id)
        if (order == null) {
            res.status(200).sejsonnd("This is not a valid order");
            failed = false;
        } else {
            res.status(200).json(order.status);
            failed = false;
        }
    }
    if (failed) {
        res.status(500).send("Something wrong happened");
    }

});

function checkOrderPlacement() {
    Request.get({
        uri : "http://admin:admin@localhost:8161/api/message?destination=queue://placeOrder/" + port + "&oneShot=true"
    },function(err, response, body) {
            if (!err && body) {
                console.log("Found an order to process");
                var data = JSON.parse(body);
                var purchaseid = data.purchaseid;
                var products = data.products;
                console.log("Purchase " + purchaseid);
                console.log("Products " + products);
                productsToOrder = [];
                productsAvaliable.forEach(prod => {
                    if (products.includes(prod.name)) {
                        productsToOrder.push(prod.name);
                    }
                });
                if (productsToOrder.length == 0) {
                    console.log("Order had no valid products");
                } else {
                    var order = {};
                    order.product = products[0];
                    order.status = "In preparation";
                    order.id = makeid(12);
                    Request.put({
                        uri : "http://localhost:9000/master/purchase/updateOrder",
                        json: {order: order, purchaseid: purchaseid}
                    }, function(err, response, body) {
                            if (!body.error) {
                                console.log("Preparing order " + order.id);
                                setTimeout(function () {
                                    Request.put({
                                        uri : "http://localhost:9000/master/purchase/ready/" + order.id
                                    }, function(err, response, body) {
                                            var data = JSON.parse(body);
                                            console.log("Order " + data.order.id + " was fullfilled");
                                        }
                                    );
                                }, random(10000, 20000, 1));
                            }
                        }
                    );
                }
            }
            checkOrderPlacement();
        }
    );
    
}

Request.post({
    uri : "http://localhost:9000/master/supplier",
    json: {name: port, host: "localhost", port: port}
}, function(err, res, body) {
        if (!err) {
            Request.post({
                uri : "http://localhost:9000/master/product",
                json: {products: productsAvaliable}
            }, function(err, res, body) {
                if (!err) {
                    app.use('/supplier', router);
                    
                    app.listen(port, function () {
                        console.log("Listening on " + port);
                        checkOrderPlacement();
                    });
                } else {
                    console.log("Unable to subscrive products to master server");
                }
            });
        } else {
            console.log("Unable to subscrive to master server");
        }
    }
);
