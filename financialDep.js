var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var Request = require('request');

var router = express.Router();
app.use('/financial', router);

function random(min, max, unityParts) {

    var value = Math.random() * (max - min) + min;
    var roundedValue = Math.round(value * unityParts) / unityParts;
    if (unityParts == 1) {
        console.log("Generated value is : [" + roundedValue + "]");
    }
    return roundedValue;
}

function validateOrder() {
    Request.get({
        uri : "http://admin:admin@localhost:8161/api/message?destination=queue://validateOrder&oneShot=true"
    },function(err, response, body) {
            if (!err && body) {
                console.log("Found an order to validate");
                var aux = random(1, 100, 100)
                var data = JSON.parse(body);
                if (aux <= 80) {
                    console.log("Purchase " + data.purchase.id + " was accepted!");
                    Request.put({
                        uri : "http://localhost:9000/master/purchase/valid/" + data.purchase.id
                    }, function(err, response, body) {
                        var data = JSON.parse(body);
                        console.log("Purchase " + data.order.id + " set to approved!");
                        setTimeout(function () {
                            data.order.orders.forEach(order => {
                                var productToSend = [order.product.name];
                                Request.post({
                                    uri : "http://admin:admin@localhost:8161/api/message?destination=queue://placeOrder/" + order.product.store,
                                    json: { purchaseid: data.order.id, products: productToSend }
                                }, function(err, response, body) {
                                    console.log(body);
                                });
                            });
                        }, random(5000, 10000, 1));
                    });
                } else {
                    console.log("Purchase " + data.purchase.id + " was not accepted!");
                    Request.put({
                        uri : "http://localhost:9000/master/purchase/invalid/" + data.purchase.id
                    }, function(err, response, body) {
                        var data = JSON.parse(body);
                        console.log("Purchase " + data.order.id + " set to not approved!");
                    });
                }
            }
            validateOrder();
        }
    );
    
}

app.listen(9001, function() {
    console.log("Listening on " + 9001);
    validateOrder()
});
