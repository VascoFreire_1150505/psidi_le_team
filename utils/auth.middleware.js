const authService  = require('../services/auth.service');
var jwthelper      = require('jwt-decode');
var UserController = require('../controllers/userController.js');

function isAuthenticated(){
    return function(req, res, next){
        //Get the token from the request
        let token = getTokenFromRequest(req);
        if(!token){
            return res.status(401).send({error:"token_not_provided", message:"You need a token to access this API"});
        }
        
        //Verify if the provided token is valid
        authService.verifyToken(token, (err, decoded) => {
            if(err){
                return res.status(401).json({error: 'invalid_token', message: err.message});
            }
            
            var check = 0;
            UserController.getUsersArray().forEach(user => {
                var userName = getUser(token).name;
                if (user.name == userName) {
                    check++;
                    req.user = user;
                    next();
                }
            });
            if (check == 0) {
                res.status(401).json({ error: 'invalid_token', message: 'Token is not valid.' });
            }
        });
    };
}

function getTokenFromRequest(req){
    //Try to get from Headers
    let token = req.headers.authorization;
    if(token){
        //It is expected to be something like "Bearer <token>"
        let parts = token.split(' ');
        return parts.length == 2 ? parts[1] : null;
    }
    
    //Default to query variable
    return req.query.token;
}

function getUser(token) {
        
    if (!token) {
        return null;
    }

    return jwthelper(token);
}

module.exports.isAuthenticated = isAuthenticated;
module.exports.getTokenFromRequest = getTokenFromRequest;
module.exports.getUser = getUser;
