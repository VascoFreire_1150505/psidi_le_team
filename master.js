var express = require('express');
var app = express();
var port = 9000;
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var UserController = require('./controllers/userController.js');
var ProductController = require('./controllers/productController.js');
var SupplierController = require('./controllers/supplierController.js');
var PurchaseController = require('./controllers/purchaseController.js');
const auth         = require('./utils/auth.middleware');

var router = express.Router();

router.route('/user')
    .post(UserController.createUser);
router.route('/login')
    .post(UserController.login);
router.route('/product')
    .get(ProductController.getProducts)
    .post(ProductController.addSupplierProducts);
router.route('/product/:name')
    .get(ProductController.getProductsByName);
router.route('/product/:product/:supplier')
    .get(auth.isAuthenticated(), ProductController.getProductByNameSupplier);
router.route('/purchase')
    .post(auth.isAuthenticated(), PurchaseController.makeOrderHyper);
router.route('/purchase/cheapest')
    .post(auth.isAuthenticated(), PurchaseController.makeOrder);
router.route('/purchase/updateOrder')
    .put(PurchaseController.addOrderToPurchase);
router.route('/purchase/status/:id')
    .get(auth.isAuthenticated(), PurchaseController.getOrderById);
router.route('/purchase/history')
    .get(auth.isAuthenticated(), PurchaseController.getOrdersHistory);
router.route('/purchase/history/:product')
    .get(auth.isAuthenticated(), PurchaseController.getOrderByProduct);
router.route('/purchase/valid/:id')
    .put(PurchaseController.putOrderValid);
router.route('/purchase/invalid/:id')
    .put(PurchaseController.putOrderInvalid);
router.route('/purchase/ready/:id')
    .put(PurchaseController.putOrderReady);
router.route('/purchase/pickedup/:id')
    .put(auth.isAuthenticated(), PurchaseController.putOrderPickedUp);
router.route('/supplier')
    .post(SupplierController.addSupplier);
router.route('/quote')
    .get(ProductController.getQuotations);

app.use('/master', router);

app.listen(port, function() {
    console.log("Listening on " + port);
});
