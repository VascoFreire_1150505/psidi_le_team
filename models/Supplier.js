const mongoose        = require('mongoose');
const Schema          = mongoose.Schema;

var SupplierSchema = new Schema({
    name : {
        type     : String,
        required : [true, "Name is mandatory"]
    },
    host : {
        type     : String,
        required : [true, "Host is mandatory"]
    },
    port : {
        type     : String,
        required : [true, "Port is mandatory"]
    }
});

module.exports = mongoose.model('Supplier', SupplierSchema);