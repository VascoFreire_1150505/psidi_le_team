const mongoose        = require('mongoose');
const Schema          = mongoose.Schema;

var ProductSchema = new Schema({
    name : {
        type     : String,
        required : [true, "Name is mandatory"]
    },
    barcode : {
        type     : String,
        required : [true, "Barcode is mandatory"]
    },
    price : {
        type     : Number,
        required : [true, "Price is mandatory"]
    },
    type : {
        type     : String,
        enum: ['MAT_ESCRIT', 'HIGIENE'],
        required : [true, "Store is mandatory"]
    },
    store : {
        type     : String,
        required : true,
        required : [true, "Store is mandatory"]
    }
});

module.exports = mongoose.model('Product', ProductSchema);