const mongoose        = require('mongoose');
const Schema          = mongoose.Schema;
const bcrypt          = require('bcryptjs');

var UserSchema = new Schema({
    name : {
        type     : String,
        required : [true, "Name is mandatory"]
    },
    password : {
        type     : String,
        required : [true, "Password is mandatory"],
        validate : {
            validator: function(v) {
                return v.length >= 8;
            },
            message : 'Password must have at least 8 characters'
        },
    },
    type: {
        type     : String,
        required : true,
        enum     : ['A', 'B', 'C']
    }
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);