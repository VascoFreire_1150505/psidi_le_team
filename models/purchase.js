const mongoose        = require('mongoose');
const Schema          = mongoose.Schema;

var PurchaseSchema = new Schema({
    id : {
        type     : String,
        required : [true, "id is mandatory"]
    },
    userName : {
        type     : String,
        required : [true, "User Name is mandatory"]
    },
    productslist : {
        type     : [String],
        required : [true, "At least a product is necessary"]
    },
    status : {
        type     : String,
        enum: ['in preparation', 'fullfiled', 'picked up'],
        required : [false, "status Name is mandatory"]
    },    
    date: {
        type    : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('Purchase', PurchaseSchema);