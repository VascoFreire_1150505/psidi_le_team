var Supplier = require('../models/supplier');
var Request = require('request');

var suppliers = [];

module.exports = {
    getSuppliersArray: function() {
        return suppliers;
    },
    getSupplierByName: function(name) {
        var supp = null;
        console.log(name);
        suppliers.forEach(sup => {
            if (sup.name == name) {
                supp = sup;
            }
        });
        return supp;
    },
    addSupplier: function(req, res) {
        var supplier = new Supplier();
        if (req.body == undefined || req.body.name == undefined || req.body.host == undefined || req.body.port == undefined) {
            res.status(200).json({ error: 'invalid_supplier', message: 'Supplier not created! Name, host and port are mandatory!'});
        }
        supplier.name = req.body.name;
        supplier.host = req.body.host;
        supplier.port = req.body.port;
        var aux = false;
        suppliers.forEach(sup => {
            if (supplier.name == sup.name) {
                aux = true;
            }
        });
        if (!aux) {
            suppliers.push(supplier);
            res.status(200).json({ message: 'Supplier created!', supplier: supplier });
        } else {
            res.status(200).json({ message: 'Supplier not created!' });
        }
    }

}
