var Purchase = require('../models/purchase');
var Request = require('request');
var SupplierController = require('../controllers/supplierController.js');
var ProductController = require('../controllers/productController.js');
const auth         = require('../utils/auth.middleware');

var url = "http://localhost:9000/master/";
var purchases = [];

function getLowestPriceProduct(prods) {
    var bestProd = prods[0];
    prods.forEach(prod => {
        if (bestProd.price > prod.price) {
            bestProd = prod;
        }
    });
    return bestProd;
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function addOrderToPurchase(purchaseid, order) {
    purchases.forEach(purchase => {
        if (purchase.id == purchaseid) {
            purchase.orders.push(order);
        }
    });
}

function updateOrderToPurchase(purchaseid, order) {
    purchases.forEach(purchase => {
        if (purchase.id == purchaseid) {
            purchase.orders.forEach(o => {
                if (order.product == o.product.name) {
                    o.id = order.id;
                    o.status = "Being treated by the supplier";
                }
            });
        }
    });
}

function changePurchaseStatus(purchaseid, status) {
    purchases.forEach(purchase => {
        if (purchase.id == purchaseid) {
            purchase.status = status;
        }
    });
}

function changeTotalPrice(purchaseid, price) {
    purchases.forEach(purchase => {
        if (purchase.id == purchaseid) {
            purchase.totalprice = price;
        }
    });
}

function numberUserPurchases(username) {
    userPurchases = [];
    purchases.forEach(purchase => {
        if (purchase.user == username) {
            userPurchases.push(purchase);
        }
    });
    return userPurchases;
}

module.exports = {
    getOrderById: function(req, res) {
        var id = req.params.id;
        var purchaseToReturn = null
        purchases.forEach(pur => {
            if (pur.id == id) {
                var link1 = {rel: "status", href: url+"purchase/status/"+pur.id}
                var link2 = {rel: "orderready", href: url+"purchase/pickedup/"+pur.id}
                var p = pur;
                p.links = [link1,link2];
                purchaseToReturn = p;
            }
        });
        if (purchaseToReturn == null) {
            res.status(400).json({ error: 'invalid_purchase', message: 'Purchase does not exists!'});
        } else {
            var link1 = {rel: "products", href: url+"products"}
            res.status(200).json({ order: purchaseToReturn, links: [link1] });
        }
    },
    getOrdersHistory: function(req, res) {
        var userName = auth.getUser(auth.getTokenFromRequest(req)).name;
        var purchasesToReturn = []
        purchases.forEach(pur => {
            if (userName == pur.user) {
                var link = {rel: "status", href: url+"purchase/status/"+pur.id}
                var p = pur;
                p.links = [link];
                purchasesToReturn.push(p);
            }
        });
        var link1 = {rel: "products", href: url+"products"}
        var link2 = {rel: "self", href: url+"purchase/history"}
        res.status(200).json({ purchases: purchasesToReturn, links: [link1, link2] });
    },
    getOrderByProduct: function(req, res) {
        var prod = req.params.product;
        var userName = auth.getUser(auth.getTokenFromRequest(req)).name;
        var purchasesToReturn = []
        purchases.forEach(pur => {
            if (pur.productslist.includes(prod) && userName == pur.userName) {
                purchasesToReturn.push(pur);
            }
        });
        res.status(200).json({ orders: purchasesToReturn });
    },
    makeOrder: function(req, res) {
        var products = req.query.products.split(",");
        if (products == null || products.length <= 0) {
            res.status(400).json({ error: 'invalid_products', message: 'Must send products!'});
        } else {
            var type = auth.getUser(auth.getTokenFromRequest(req)).role;
            var orderid = makeid(8);
            var totalprice = 0;
            var nPurchases = numberUserPurchases(username).length;
            var username = auth.getUser(auth.getTokenFromRequest(req)).name;
            var link1 = {rel: "status", href: url+"purchase/status/"+orderid}
            var link2 = {rel: "prev", href: url+"product"}
            var purchase = {id: orderid, user: username, products: products, orders: [], totalprice: totalprice, status:"Waiting for validation"};
            purchases.push(purchase);
            res.status(200).json({ message: 'Order was sent!', orderid: purchase.id, links: [link1,link2] });
            products.forEach(product => {
                var prods = [];
                var cheapestproduct = null;
                var prods = ProductController.getProductsByNameArray(product);
                var neworder = {};
                neworder.id = 0;
                if (prods == null || prods.length <= 0) {
                    neworder.product = product;
                    neworder.status = "Not in stock";
                } else {
                    neworder.status = "Waiting";
                    cheapestproduct = getLowestPriceProduct(prods);
                    totalprice = totalprice + cheapestproduct.price;
                    neworder.product = cheapestproduct;
                }
                addOrderToPurchase(purchase.id, neworder);
            });
            console.log("Total price = " + totalprice);
            console.log("Number of purchases = " + nPurchases);
            console.log("User type = " + type);
            changeTotalPrice(purchase.id, totalprice);
            if (((type == "A" || type == "B") && totalprice < 5000) || (nPurchases == 0 && totalprice < 1000)) {
                console.log("Purchase " + purchase.id + " sent to supplier");
                purchase.orders.forEach(order => {
                    var productToSend = [order.product.name];
                    Request.post({
                        uri : "http://admin:admin@localhost:8161/api/message?destination=queue://placeOrder/" + order.product.store,
                        json: { purchaseid: purchase.id, products: productToSend }
                    }, function(err, response, body) {
                        console.log(body);
                    });
                });
            } else {
                console.log("Purchase " + purchase.id + " sent to validation");
                Request.post({
                    uri : "http://admin:admin@localhost:8161/api/message?destination=queue://validateOrder",
                    json: { purchase: purchase }
                }, function(err, response, body) {
                    console.log(body);
                });
            }
        }
    },
    makeOrderHyper: function(req, res) {
        var products = req.query.products.split(",");
        var supplier = req.query.supplier;
        if (products == null || products.length <= 0 || supplier == null) {
            res.status(400).json({ error: 'invalid_data', message: 'Must send products and supplier!'});
        } else {
            var type = auth.getUser(auth.getTokenFromRequest(req)).role;
            var orderid = makeid(8);
            var totalprice = 0;
            var nPurchases = numberUserPurchases(username).length;
            var username = auth.getUser(auth.getTokenFromRequest(req)).name;
            var link1 = {rel: "status", href: url+"purchase/status/"+orderid}
            var link2 = {rel: "prev", href: url+"quote?products="+req.query.products}
            var purchase = {id: orderid, user: username, products: products, orders: [], totalprice: totalprice, status:"Waiting for validation"};
            purchases.push(purchase);
            res.status(200).json({ message: 'Order was sent!', orderid: purchase.id, links: [link1,link2] });
            products.forEach(product => {
                var prods = [];
                var prods = ProductController.getProductsByNameArray(product);
                prodToOrder = null;
                prods.forEach(p => {
                    if (p.store == supplier) {
                        prodToOrder = p;
                    }
                });
                var neworder = {};
                neworder.id = 0;
                if (prodToOrder == null) {
                    neworder.product = product;
                    neworder.status = "Not in stock";
                } else {
                    neworder.status = "Waiting";
                    totalprice = totalprice + prodToOrder.price;
                    neworder.product = prodToOrder;
                }
                addOrderToPurchase(purchase.id, neworder);
            });
            console.log("Total price = " + totalprice);
            console.log("Number of purchases = " + nPurchases);
            console.log("User type = " + type);
            changeTotalPrice(purchase.id, totalprice);
            if (((type == "A" || type == "B") && totalprice < 5000) || (nPurchases == 0 && totalprice < 1000)) {
                console.log("Purchase " + purchase.id + " sent to supplier");
                purchase.orders.forEach(order => {
                    var productToSend = [order.product.name];
                    Request.post({
                        uri : "http://admin:admin@localhost:8161/api/message?destination=queue://placeOrder/" + order.product.store,
                        json: { purchaseid: purchase.id, products: productToSend }
                    }, function(err, response, body) {
                        console.log(body);
                    });
                });
            } else {
                console.log("Purchase " + purchase.id + " sent to validation");
                Request.post({
                    uri : "http://admin:admin@localhost:8161/api/message?destination=queue://validateOrder",
                    json: { purchase: purchase }
                }, function(err, response, body) {
                    console.log(body);
                });
            }
        }
    },
    addOrderToPurchase: function(req, res) {
        var neworder = {};
        if (req.body == null) {
            res.status(400).json({ error: 'invalid_order', message: 'Invalid order!'});
        } else {
            var order = req.body.order;
            var purchaseid = req.body.purchaseid;
            if (!order || !purchaseid){
                res.status(400).json({ error: 'invalid_order', message: 'Invalid order!'});
            } else {
                neworder.id = order.id;
                neworder.product = order.product;
                neworder.status = order.status;
                updateOrderToPurchase(purchaseid, neworder);
                res.status(200).json({ message: 'Order added!'});
            }
        }
    },
    putOrderValid: function(req, res) {
        var id = req.params.id;
        var purchaseToReturn = null
        purchases.forEach(pur => {
            if (pur.id == id) {
                purchaseToReturn = pur;
            }
        });
        if (purchaseToReturn == null) {
            res.status(400).json({ error: 'invalid_purchase', message: 'Purchase does not exists!'});
        } else {
            if (purchaseToReturn == "Fullfilled" || purchaseToReturn == "Picked up") {
                res.status(200).json({ error: 'invalid_purchase', message: 'Purchase already fullfilled!' });
            } else {
                purchaseToReturn.status = "Approved";
                purchases.forEach(pur => {
                    if (pur.id == id) {
                        pur = purchaseToReturn;
                    }
                });
                res.status(200).json({ order: purchaseToReturn });
            }
        }
    },
    putOrderInvalid: function(req, res) {
        var id = req.params.id;
        var purchaseToReturn = null
        purchases.forEach(pur => {
            if (pur.id == id) {
                purchaseToReturn = pur;
            }
        });
        if (purchaseToReturn == null) {
            res.status(400).json({ error: 'invalid_purchase', message: 'Purchase does not exists!'});
        } else {
            if (purchaseToReturn == "Fullfilled" || purchaseToReturn == "Picked up") {
                res.status(400).json({ error: 'invalid_purchase', message: 'Purchase already fullfilled!' });
            } else {
                purchaseToReturn.status = "Not approved";
                purchases.forEach(pur => {
                    if (pur.id == id) {
                        pur = purchaseToReturn;
                    }
                });
                res.status(200).json({ order: purchaseToReturn });
            }
        }
    },
    putOrderReady: function(req, res) {
        var id = req.params.id;
        var purchaseToReturn = null
        purchases.forEach(pur => {
            pur.orders.forEach(order => {
                if (order.id == id) {
                    purchaseToReturn = order;
                }
            });
        });
        if (purchaseToReturn == null) {
            res.status(200).json({ error: 'invalid_purchase', message: 'Purchase does not exists!'});
        } else {
            if (purchaseToReturn == "Fullfilled" || purchaseToReturn == "Picked up") {
                res.status(400).json({ message: 'Purchase already fullfilled!' });
            } else {
                purchaseToReturn.status = "Fullfilled";
                purchases.forEach(pur => {
                    var aux = true;
                    pur.orders.forEach(order => {
                        if (order.id == id) {
                            order = purchaseToReturn;
                        }
                        if (!order.status.includes("Fullfilled")) {
                            aux = false;
                        }
                    });
                    if (aux == true) {
                        pur.status = "Fullfilled";
                    }
                });
                res.status(200).json({ order: purchaseToReturn });
            }
        }
    },
    putOrderPickedUp: function(req, res) {
        var id = req.params.id;
        var purchaseToReturn = null;
        purchases.forEach(pur => {
            if (pur.id == id) {
                purchaseToReturn = pur;
            }
        });
        if (purchaseToReturn == null) {
            res.status(400).json({ error: 'invalid_purchase', message: 'Purchase does not exists!'});
        } else {
            if (!purchaseToReturn.status.includes("Fullfilled")) {
                res.status(200).json({message: 'Purchase is not ready to be picked up!' });
            } else {
                purchaseToReturn.status = "Picked up";
                purchases.forEach(pur => {
                    if (pur.id == id) {
                        pur = purchaseToReturn;
                    }
                });
                res.status(200).json({ order: purchaseToReturn, links: [{rel: "next", href: url+"product"}] });
            }
        }
    }
}
