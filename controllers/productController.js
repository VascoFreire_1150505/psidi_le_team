var Product = require('../models/product');
var Request = require('request');
var SupplierController = require('../controllers/supplierController.js');
var url = "http://localhost:9000/master/";
var products = [];

function getLowestPriceProduct(prods) {
    var bestProd = prods[0];
    prods.forEach(prod => {
        if (bestProd.price > prod.price) {
            bestProd = prod;
        }
    });
    return bestProd;
}

function getProductByNameSupplierArray(sup) {
    prods = [];
    products.forEach(p => {
        if (sup.name.toUpperCase().includes(p.store.toUpperCase())) {
            prods.push(p);
        }
    });
    return prods;
}

module.exports = {
    getProductsArray: function() {
        return products;
    },
    getProducts: function(req, res) {
        var returnData = {};
        var prods = [];
        var prodsaux = [];
        products.forEach(p => {
            if (!prodsaux.includes(p.name)) {
                prodsaux.push(p.name);
                var prod = {};
                prod.prodname = p.name;
                var link1 = {rel: "cheapestorder", href: url+"purchase/cheapest?products="+p.name};
                var link2 = {rel: "quote", href: url+"quote?products="+p.name};
                var links = [link1,link2];
                prod.links = links;
                prods.push(prod);
            }
        });
        returnData.products = prods;
        return res.status(200).json({returnData, links: [{rel:"self",hrel:url+"product"}]});
    },
    getProductsByNameArray: function(prodName) {
        var returnProds = [];
        products.forEach(prod => {
            if (prod.name.toUpperCase().includes(prodName.toUpperCase())) {
                returnProds.push(prod);
            }
        });
        return returnProds;
    },
    getProductsByName: function(req, res) {
        var returnProds = [];
        products.forEach(prod => {
            if (prod.name.toUpperCase().includes(req.params.name.toUpperCase())) {
                returnProds.push(prod);
            }
        });
        res.status(200).json(returnProds);
    },
    getProductByNameSupplier: function(req, res) {
        var sup = SupplierController.getSupplierByName(req.params.supplier);
        if (sup == null) {
            res.status(400).json({ error: 'invalid_supplier', message: 'Supplier does not exists!'});
        } else {
            prods = [];
            products.forEach(p => {
                if (sup.name == p.store) {
                    prods.push(p);
                }
            });
            res.status(200).json(prods);
        }
    },
    addSupplierProducts: function(req, res) {
        var newProducts = req.body.products;
        if (!newProducts || newProducts.length <= 0) {
            res.status(400).json({ error: 'invalid_products', message: 'No products to add!'});
        } else {
            newProducts.forEach(prod => {
                products.push(prod);
            });
            res.status(200).json({message: 'Products added!'});
        }
    },
    getQuotations: function(req, res) {
        var quotations = [];
        var wantedProds = [];
        wantedProds = req.query.products.split(",");
        if (wantedProds.length <= 0) {
            res.status(400).json({ error: 'invalid_products', message: 'No products to quote!'});
        } else {
            var suppliers = SupplierController.getSuppliersArray();
            suppliers.forEach(sup => {
                var supname = sup.name;
                var supProds = getProductByNameSupplierArray(sup);
                var newQuote = {};
                var prods = [];
                var totalPrice = 0;
                var queryprods = "";
                var links = [];
                supProds.forEach(prod => {
                    if (wantedProds.includes(prod.name)) {
                        var p = {};
                        queryprods = queryprods + prod.name;
                        p.name = prod.name;
                        p.price = prod.price;
                        prods.push(p);
                        totalPrice = totalPrice + p.price;
                    }
                });
                if (prods.length > 0) {
                    var link = {};
                    link.rel = "purchase";
                    link.href = url + "purchase?products=" + queryprods + "&supplier=" + supname;
                    links.push(link);
                    newQuote.products = prods;
                    newQuote.totalPrice = totalPrice;
                    newQuote.totalPrice = totalPrice;
                    newQuote.links = links;
                    quotations.push(newQuote);
                }
            });
            res.status(200).json({quotations: quotations, links: [{rel:"self", href:url+"quote?products="+req.query.products}]});
        }
    }
}
