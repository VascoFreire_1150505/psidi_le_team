var User = require('../models/user');
const authService = require('../services/auth.service');
//const bcrypt = require('bcrypt');
var userAdmin1 = new User();
var userAdmin2 = new User();
var userAdmin3 = new User();
userAdmin1.name = "wpsadmin1";
userAdmin1.password = "P@ssword";
userAdmin1.type = "A";
userAdmin2.name = "wpsadmin2";
userAdmin2.password = "P@ssword";
userAdmin2.type = "B";
userAdmin3.name = "wpsadmin3";
userAdmin3.password = "P@ssword";
userAdmin3.type = "C";

var users = [userAdmin1,userAdmin2,userAdmin3];

module.exports = {
    getUsersArray: function() {
        return users;
    },
    getUsers: function(req, res) {
        res.status(200).json(users);
    },
    createUser: function(req, res) {
        var user = {};
        if (req.body == undefined || req.body.name == undefined || req.body.password == undefined) {
            res.status(400).json({ error: 'invalid_user', message: 'User not created! Name and Password are mandatory!'});
        }
        user.name = req.body.name;
        user.password =req.body.password; //bcrypt.hashSync(req.body.password, 8);
        user.type = "C";
        users.push(user);
        res.status(200).json({ message: 'User created!', user: user });
    },
    login:function(req, res) {
        var check = 0;
        users.forEach(user => {
            if (user.name == req.body.user) {
                let token = authService.signToken(user);
                check++;
                res.status(200).json(token);
            }
        });
        if (check == 0) {
            res.status(401).json({ error: 'invalid_user', message: 'User and/or password are not valid.' });
        }
        return res;
    }
}
